# Deploying using Docker

## Deploy Performance Management System

```
docker-compose up -d --build
```

## How to access static file

1. Get the docker ip

```
docker-machine ip default
```

For example: 192.168.99.100

2. Access staticFile and API Endpoint

- Endpoint

```
ip:<nginx port>/<api>/<Endpoint>
```
```
For Example - 192.168.99.100:8000/api/
```

- Static file

```
ip:<nginx port>
```

```
For Example - 192.168.99.100:8000
```
