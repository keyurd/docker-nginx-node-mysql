const request = require('supertest');
const app = require('../app/app');
const chai = require('chai');
const should = chai.should();
const config = require('../app/config/config');
const expect = chai.expect;
const Sequelize = require('Sequelize');
const models = require('../app/models/index');

const jwt = require('jsonwebtoken');
let authToken = "Bearer " + jwt.sign({}, config.token.secret);

exports.authentication = describe('Authentication Tests', () => {
  describe('/users', () => {
    describe('POST', () => {
      it('it should return a 401 and error response without authorization', (done) => {
        request(app)
          .post('/'+config.apiVersion+'/users')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send({
            'firstName':'John',
            'lastName': 'Doe',
            'emailAddress': 'john.doe@gmail.com'
          })
          .end((err, res) => {
            res.status.should.equal(401);
            res.body.should.have.property("errors");
            res.body.errors.should.be.a("array");
            done(err);
          });
      });
    });
    describe('GET', () => {
      it('it should return a 401 and error response without authorization', (done) => {
        request(app)
          .post('/'+config.apiVersion+'/users')
          .end((err, res) => {
            res.status.should.equal(401);
            res.body.should.have.property("errors");
            res.body.errors.should.be.a("array");
            done(err);
          });
      })
    });
    describe('DELETE', () => {
      it('it should return a 401 and error response without authorization', (done) => {
        request(app)
          .delete('/'+config.apiVersion+'/users/1')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .end((err, res) => {
            res.status.should.equal(401);
            res.body.should.have.property("errors");
            res.body.errors.should.be.a("array");
            done(err);
          });
      });
    });
    describe('PUT', () => {
      it('it should return a 401 and error response without authorization', (done) => {
        request(app)
          .put('/'+config.apiVersion+'/users/1')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send({
            'firstName':'John',
            'lastName': 'Doe',
            'emailAddress': 'john.doe@gmail.com'
          })
          .end((err, res) => {
            res.status.should.equal(401);
            res.body.should.have.property("errors");
            res.body.errors.should.be.a("array");
            done(err);
          });
      });
    });
    describe('GET', () => {
      it('it should return a 200 and data with proper authorization', (done) => {
        request(app)
          .get('/'+config.apiVersion+'/users')
          .set("authorization",authToken)
          .expect(200)
          .end((err, res) => {
            res.status.should.equal(200);
            res.body.should.be.a("array");
            done();
          });
      })
    })
  });
  describe('/groups', () => {
    describe('POST', () => {
      it('it should return a 401 and error response without authorization', (done) => {
        request(app)
          .post('/'+config.apiVersion+'/groups')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send({
            'title':'Group A',
            'description': 'Desc',
            'slug': 'group-a'
          })
          .end((err, res) => {
            res.status.should.equal(401);
            res.body.should.have.property("errors");
            res.body.errors.should.be.a("array");
            done(err);
          });
      });
    });
    describe('GET', () => {
      it('it should return a 401 and error response without authorization', (done) => {
        request(app)
          .post('/'+config.apiVersion+'/groups')
          .end((err, res) => {
            res.status.should.equal(401);
            res.body.should.have.property("errors");
            res.body.errors.should.be.a("array");
            done(err);
          });
      })
    });
    describe('DELETE', () => {
      it('it should return a 401 and error response without authorization', (done) => {
        request(app)
          .delete('/'+config.apiVersion+'/groups/1')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .end((err, res) => {
            res.status.should.equal(401);
            res.body.should.have.property("errors");
            res.body.errors.should.be.a("array");
            done(err);
          });
      });
    });
    describe('PUT', () => {
      it('it should return a 401 and error response without authorization', (done) => {
        request(app)
          .put('/'+config.apiVersion+'/groups/1')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send({
            'firstName':'John',
            'lastName': 'Doe',
            'emailAddress': 'john.doe@gmail.com'
          })
          .end((err, res) => {
            res.status.should.equal(401);
            res.body.should.have.property("errors");
            res.body.errors.should.be.a("array");
            done(err);
          });
      });
    });
    describe('GET', () => {
      it('it should return a 200 and data with proper authorization', (done) => {
        request(app)
          .get('/'+config.apiVersion+'/groups')
          .set("authorization",authToken)
          .expect(200)
          .end((err, res) => {
            res.status.should.equal(200);
            res.body.should.be.a("array");
            done();
          });
      })
    })
  });
  describe('permissions', () => {
    describe('POST', () => {
      it('it should return a 401 and error response without authorization', (done) => {
        request(app)
          .post('/'+config.apiVersion+'/permissions')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send({
            'title':'Permission A',
            'description': 'Desc'
          })
          .end((err, res) => {
            res.status.should.equal(401);
            res.body.should.have.property("errors");
            res.body.errors.should.be.a("array");
            done(err);
          });
      });
    });
    describe('GET', () => {
      it('it should return a 401 and error response without authorization', (done) => {
        request(app)
          .post('/'+config.apiVersion+'/permissions')
          .end((err, res) => {
            res.status.should.equal(401);
            res.body.should.have.property("errors");
            res.body.errors.should.be.a("array");
            done(err);
          });
      })
      it('it should return a 200 and data with authorization', (done) => {
        request(app)
          .get('/'+config.apiVersion+'/permissions')
          .set("authorization",authToken)
          .expect(200)
          .end((err, res) => {
            res.status.should.equal(200);
            res.body.should.be.a("array");
            done();
          });
      })
    });
    describe('DELETE', () => {
      it('it should return a 401 and error response without authorization', (done) => {
        request(app)
          .delete('/'+config.apiVersion+'/permissions/1')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .end((err, res) => {
            res.status.should.equal(401);
            res.body.should.have.property("errors");
            res.body.errors.should.be.a("array");
            done(err);
          });
      });
    });
    describe('PUT', () => {
      it('it should return a 401 and error response without authorization', (done) => {
        request(app)
          .put('/'+config.apiVersion+'/permissions/1')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send({
            'firstName':'John',
            'lastName': 'Doe',
            'emailAddress': 'john.doe@gmail.com'
          })
          .end((err, res) => {
            res.status.should.equal(401);
            res.body.should.have.property("errors");
            res.body.errors.should.be.a("array");
            done(err);
          });
      });
    });
  });
  describe('/authenticate', () => {
    describe('POST', () => {
      it('it should return the user and a newly assigned token when the user exists in the database', (done) => {
        try{
          done();
          models.User.findOrCreate({
            where: {emailAddress: "john.doe@gmail.com", password: "justForTesting"}, 
            defaults: {firstName: "John", lastName: "Doe", password: "justForTesting"}
          }).spread((user, created) => {
              request(app)
                .post('/'+config.apiVersion+'/authenticate')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({
                  'emailAddress': 'john.doe@gmail.com',
                  'password': 'justForTesting'
                })
                .end((err, res) => {
                  res.status.should.equal(200);
                  res.body.should.have.property("errors");
                  res.body.errors.should.be.a("array");
                  done(err);
                });
            });
        }
        catch(error){
          console.error(error);
          done(error);
        }
      });
    });
  });
});