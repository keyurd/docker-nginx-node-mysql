# Performance Management System


[ ![Codeship Status for TeamIllegalSkillsException/minimum-viable-product](https://app.codeship.com/projects/8c6b9ca0-38b5-0135-9657-7251e562a337/status?branch=master)](https://app.codeship.com/projects/227930)


To start the api server

```
npm start
```

How to start test

```
npm test
```

To fix uncaughtException: listen EADDRINUSE error on Windows using bash/cmd:
```
taskkill //F //IM node.exe
```
