const Errors = require('./errors');
const jwt = require('jsonwebtoken');
const config = require('./config/config');

module.exports.requireApiKey = (req, res, next) => {
  let clientIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  // TODO: Check user has api key
  next();
};

module.exports.requireToken = (req, res, next) => {
  let bearerHeader = req.headers['authorization'];
  if (typeof bearerHeader !== 'undefined') {
    req.token = bearerHeader.split(' ')[1];
    jwt.verify(req.token, config.token.secret, function(err, decoded) {
      if(err){
        Logger.error(err);
        return next(new Errors.InternalServerError('Error authenticating'));
      }
      else{
        next();
      }
    });
  }
  else{
    notAuthorizedResponse(req, res, next);
  }
};

module.exports.requireEpilogueAuth = (req, res, context) => {
  return new Promise(function(resolve, reject) {
    let bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
      req.token = bearerHeader.split(' ')[1];
      jwt.verify(req.token, config.token.secret, (err, decoded) => {
        if(err){
          Logger.info(err);
          notAuthorizedResponse(req, res, null);
          resolve(context.stop);
        }
        else{
          resolve(context.continue);
        }
      });
    }
    else{
      resolve(context.stop);
      notAuthorizedResponse(req, res, null);
    }
  });
}

notAuthorizedResponse = (req, res, next) =>{
  res.status(401);
  res.json({
    errors: [{
      status: '401',
      'message': 'Unauthorized'
    }]
  });
};
