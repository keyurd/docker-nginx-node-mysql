module.exports = function NotFound(message) {
  Error.captureStackTrace(this, this.constructor);

  this.name = this.constructor.name;
  this.message = message || 'Resource Not Found';
  this.statusCode = 404;
};

require('util').inherits(module.exports, Error);
