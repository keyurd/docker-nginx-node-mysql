const epilogue = require('epilogue');
const permissionsModel = require('../../models/index').models.Permission;
const config = require('../../config/config');

let permissions = epilogue.resource({
  model: permissionsModel,
  endpoints: [
    '/'+config.apiVersion+'/permissions',
    '/'+config.apiVersion+'/permissions/:permissionId'
  ],
  associations: true,
  search: [{
    operator: '$eq',
    param: 'permissionid',
    attributes: ['permissionId']
  }, ],
});

module.exports = permissions;