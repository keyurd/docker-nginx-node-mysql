const app = require('../../app');
const epilogue = require('epilogue');
const models = require('../../models/index').models;
const config = require('../../config/config');

let userQuantitativeData = epilogue.resource({
  model: models.UserQuantitativeData,
  endpoints: [
    '/'+config.apiVersion+'/userQuantitativeData',
    '/'+config.apiVersion+'/userQuantitativeData/:quantitativeDataId'
  ],
  associations: true,
  search: [{
    operator: '$eq',
    param: 'quantitativeData',
    attributes: ['quantitativeData']
  }, ],
});

module.exports = userQuantitativeData;