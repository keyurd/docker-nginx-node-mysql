const app = require('../../app');
const epilogue = require('epilogue');
const models = require('../../models/index').models;
const config = require('../../config/config');

let users = epilogue.resource({
  model: models.User,
  endpoints: [
    '/'+config.apiVersion+'/users',
    '/'+config.apiVersion+'/users/:userId'
  ],
  associations: true,
  search: [{
    operator: '$eq',
    param: 'userId',
    attributes: ['userId']
  }, ],
});

module.exports = users;
