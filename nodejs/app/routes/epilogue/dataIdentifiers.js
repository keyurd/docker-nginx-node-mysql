const app = require('../../app');
const epilogue = require('epilogue');
const models = require('../../models/index').models;
const config = require('../../config/config');

let dataIdentifiers = epilogue.resource({
  model: models.DataIdentifier,
  endpoints: [
    '/'+config.apiVersion+'/dataIdentifiers',
    '/'+config.apiVersion+'/dataIdentifiers/:dataIdentifierId'
  ],
  associations: true,
  search: [{
    operator: '$eq',
    param: 'dataIdentifierId',
    attributes: ['dataIdentifierId']
  }, ],
});

module.exports = dataIdentifiers;