const express = require('express');
const epilogue = require('epilogue');
const sequelize = require('../../config/dbCrud.js');
const auth = require('../../authentication');

exports.initialize = (app) => {
  epilogue.initialize({
    app: app,
    sequelize: sequelize
  });

  let epilogueResources = {
    users: require('./users').all.auth((req, res, context) => {
      return auth.requireEpilogueAuth(req, res, context)
    }),
    groups: require('./groups').all.auth((req, res, context) => {
      return auth.requireEpilogueAuth(req, res, context)
    }),
    permissions: require('./permissions').all.auth((req, res, context) => {
      return auth.requireEpilogueAuth(req, res, context)
    }),
    quantitativeData: require('./quantitativeData').all.auth((req, res, context) => {
      return auth.requireEpilogueAuth(req, res, context)
    }),
    dataIdentifiers: require('./dataIdentifiers').all.auth((req, res, context) => {
      return auth.requireEpilogueAuth(req, res, context)
    }),
  }
  return epilogueResources;
};