const jwt = require('jsonwebtoken');
const config = require('../config/config');
const models = require('../models/index.js');
const sequelize = require('../config/dbCrud');
const seeds = require('../models/seeds/sequelizeSeeds');

module.exports.syncDB = function(req, res, next) {
  models.initialize().then(sequelize => {
    res.status(200).json({data: {message: "Tables synced"}});
  });

};

module.exports.dropDB = function(req, res, next) {
  sequelize.query('SET FOREIGN_KEY_CHECKS = 0')
  .then(() => {
    return sequelize.drop();
  })
  .then(() => {
      return sequelize.query('SET FOREIGN_KEY_CHECKS = 1')
  })
  res.status(200).json({data: {message: "Tables dropped"}});
};

module.exports.seedDB = (req, res, next) => {
  seeds.plant();
  res.status(200).json({data: {message: "Tables seeded"}});
}


