const Sequelize = require('sequelize');
const sequelize = require('../config/dbCrud.js');

// https://stackoverflow.com/questions/22958683/how-to-implement-many-to-many-association-in-sequelize

const models = {
    User: require('./user'),
    Group: require('./group'),
    UserGroup: require('./usersGroups'),
    Permission: require('./permission'),
    // GroupPermission: require('./groupsPermissions'),
    UserQuantitativeData: require('./userQuantitativeData'),
    DataIdentifier: require('./dataIdentifier')
}

exports.initialize = () => {
    Logger.debug('Initializing Sequelize models...');
    return new Promise((resolve, reject) =>{
        
        // Define associations
        // ================== User Group ===================
        models.User.belongsToMany(models.Group, {through: 'user_group', constraints: false, foreignKey: "userId" });
        models.Group.belongsToMany(models.User, {through: 'user_group', constraints: false, foreignKey: "groupId"});
        //models.User.hasMany(models.Group, {foreignKey: 'groupId', constraints: false}); // epilogue relies on these but breaks sequelize
        //models.Group.hasMany(models.User, {through: 'user_group', constraints: false}); // epilogue relies on these but breaks sequelize

        // ================== User Quantitative Data ===================
        models.User.hasMany(models.UserQuantitativeData, {foreignKey: "quantitativeDataId", constraints: false});
        models.UserQuantitativeData.belongsTo(models.User, {foreignKey: "quantitativeDataId", constraints: false});
        
        models.UserQuantitativeData.hasOne(models.DataIdentifier, {foreignKey: "dataIdentifierId", constraints: false});
        models.DataIdentifier.belongsTo(models.UserQuantitativeData, {foreignKey: "dataIdentifierId", constraints: false});
        
        // ================== Group Permission ===================
        models.Group.belongsToMany(models.Permission, {through: 'group_permission', constraints: false, foreignKey: "groupId"});
        models.Permission.belongsToMany(models.Group, {through: 'group_permission', constraints: false, foreignKey: "permissionId"});
        // models.Group.hasMany(models.Permission, {foreignKey: "permissionId", constraints: false});
        // models.Permission.hasMany(models.Group, {foreignKey: "groupId", constraints: false});

        // Sync models
        sequelize.sync({force: false});
        resolve(sequelize);
    });
}

// exports.User = models.User; // TODO: Depreciate in favor of 'models' export
// exports.Group = models.Group; // TODO: Depreciate in favor of 'models' export
// exports.Permission = models.Permission; // TODO: Depreciate in favor of 'models' export
// exports.UserGroup = models.UserGroup;// TODO: Depreciate in favor of 'models' export 
exports.models = models;
