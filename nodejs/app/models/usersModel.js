const Sequelize = require('sequelize'),
  sequelize = require('../config/dbCrud.js'),
  Logger = require('../libs/Logger');

let usersModel = sequelize.define('users', {
  userId: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  firstName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  lastName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  emailID: {
    type: Sequelize.STRING,
    allowNull: false,
    validate:{
      isEmail : true
    }
  },
  userUUID: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    allowNull: false
  }
},
{
  timestamps: true,
  freezeTableName: true
});

usersModel
  .sync() // { force: false }
  .then(function() {
    Logger.debug('Successfully synced usersModel');
  }).catch(function(err) {
    // handle error
    Logger.error('Error while listening to database', err);
  });

module.exports = usersModel;
