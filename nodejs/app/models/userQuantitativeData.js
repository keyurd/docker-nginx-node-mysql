const Sequelize = require('sequelize');
const sequelize = require('../config/dbCrud.js');
const Logger = require('../libs/Logger');

let UserQuantitativeData = sequelize.define('UserQuantitativeData', {
  quantitativeDataId: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  userId: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references:{
      model: "user",
      key: "userId"
    }
  },
  dataIdentifierId: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references:{
      model: "data_identifier",
      key: "dataIdentifierId"
    }
  },
  quantitativeDataUUID: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    allowNull: false
  },
  value: {
      type: Sequelize.DOUBLE,
      allowNull: true
  }
},
{
  timestamps: true,
  freezeTableName: true,
  tableName: 'user_quantitative_data',
  name: {
    singular: 'userQuantitativeData',
    plural: 'userQuantitativeData',
  }
});

module.exports = UserQuantitativeData;
