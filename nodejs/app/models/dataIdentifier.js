const Sequelize = require('sequelize');
const sequelize = require('../config/dbCrud.js');
const Logger = require('../libs/Logger');

let DataIdentifier = sequelize.define('DataIdentifier', {
  dataIdentifierId: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  description: {
    type: Sequelize.TEXT,
    defaultValue: null,
    allowNull: true
  },
  dataIdentifierUUID: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    allowNull: false
  }
},
{
  timestamps: true,
  freezeTableName: true,
  tableName: 'data_identifier',
  name: {
    singular: 'dataIdentifier',
    plural: 'dataIdentifiers',
  }
});

module.exports = DataIdentifier;
