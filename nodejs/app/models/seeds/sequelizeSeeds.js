const app = require('../../app');
const Sequelize = require('sequelize');
const sequelize = require('../../config/dbCrud.js');
const models = require('../index').models;

let groups = [
    {
        title: "Managers",
        description: "All about managers",
        slug: "managers"
    },
    {
        title: "Illegal Skills Exception",
        description: "All about illegalSkillsException",
        slug: "illegal-skills-exception"
    }
];
let users = [
    {
        firstName: 'Daniel',
        lastName: 'Reinke',
        emailAddress: 'daniel.reinke@adp.com',
        password: 'test'
    },
    {
        firstName: 'Keyur',
        lastName: 'Doshi',
        emailAddress: 'keyur.doshi@adp.com',
        password: 'test'
    },
    {
        firstName: 'Rishabh',
        lastName: 'Gupta',
        emailAddress: 'rishab.gupta@adp.com',
        password: 'test'
    }
];

exports.plant = () => {
    models.Group.create({
        title: groups[0].title,
        description: groups[0].description,
        slug: groups[0].slug
    }).then((group)=>{
        Logger.debug(group.get());
        models.Group.create({
            title: groups[1].title,
            description: groups[1].description,
            slug: groups[1].slug
        }).then((group)=>{
            Logger.debug(group.get());
            models.DataIdentifier.create({
                title: "Absences",
                description: "Absenteeism and performance are two highly correlated constructs. Highly motivated and engaged employees take in general fewer sick days (up to 37% less, according to Gallup). Additionally, absent employees are less productive and high absence rates throughout an organization is a key indicator for lower organizational performance."
            }).then(dataIdentifier=>{
                models.User.create({
                    firstName: 'Daniel',
                    lastName: 'Reinke',
                    emailAddress: 'daniel.reinke@adp.com',
                    password: "test"
                }).then((user)=>{
                    Logger.debug("user created");
                    models.Group.findById(1).then((group)=>{
                        group.setUsers(user);
                    });
                    models.UserQuantitativeData.create({
                        userId: user.userId,
                        value: (Math.floor(Math.random() * (11 - 0 + 1)) + 0),
                        dataIdentifierId: dataIdentifier.dataIdentifierId
                    });
                })
                .catch((error)=>{
                    Logger.error(error);
                }).then(()=>{
                    models.User.create({
                        firstName: 'Keyur',
                        lastName: 'Doshi',
                        emailAddress: 'keyur.doshi@adp.com',
                        password: "test"
                    }).then((user)=>{
                        Logger.debug("user created");
                        models.Group.findById(2).then((group)=>{
                            group.setUsers(user);
                        });
                        models.UserQuantitativeData.create({
                            userId: user.userId,
                            value: (Math.floor(Math.random() * (11 - 0 + 1)) + 0),
                            dataIdentifierId: dataIdentifier.dataIdentifierId
                        });
                    })
                    .catch((error)=>{
                        Logger.error(error);
                    });
                }).then(()=>{
                    models.User.create({
                        firstName: 'Rishabh',
                        lastName: 'Gupta',
                        emailAddress: 'rishabh.gupta@adp.com',
                        password: "test"
                    }).then((user)=>{
                        Logger.debug("user created");
                        models.Group.findById(2).then((group)=>{
                            user.setGroups(group);
                        });
                        models.UserQuantitativeData.create({
                            userId: user.userId,
                            value: (Math.floor(Math.random() * (11 - 0 + 1)) + 0),
                            dataIdentifierId: dataIdentifier.dataIdentifierId
                        });
                    })
                    .catch((error)=>{
                        Logger.error(error);
                    });
                });
            });
        });
        models.Permission.create(
            {title: "Test Permission", description: "Just a test permission"}
        ).then((permission)=>{
            Logger.debug("Permission created");
            group.setPermissions(permission);
        });
    });
}

exports.plantGroups = (groupsParam) => {
    return new Promise((resolve, reject)=>{
        groups.forEach(function(element) {
            models.Group.create({
                title: element.title,
                description: element.description,
                slug: element.slug
            }).then((group)=>{
                Logger.debug(group.get());
                return;
            });
        });
        resolve();
    });
}

exports.plantUsers = (usersParam) => {
    models.User.create({
        firstName: 'Daniel',
        lastName: 'Reinke',
        emailAddress: 'daniel.reinke@adp.com',
        password: "test"
    }).then((user)=>{
        Logger.debug("user created");
        models.Group.findById(1).then((group)=>{
            group.setUsers(user);
        });
    })
    .catch((error)=>{
        Logger.error(error);
    }).then(()=>{
        models.User.create({
            firstName: 'Keyur',
            lastName: 'Doshi',
            emailAddress: 'keyur.doshi@adp.com',
            password: "test"
        }).then((user)=>{
            Logger.debug("user created");
            models.Group.findById(2).then((group)=>{
                group.setUsers(user);
            });
        })
        .catch((error)=>{
            Logger.error(error);
        });
    }).then(()=>{
        models.User.create({
            firstName: 'Rishabh',
            lastName: 'Gupta',
            emailAddress: 'rishabh.gupta@adp.com',
            password: "test"
        }).then((user)=>{
            Logger.debug("user created");
            models.Group.findById(2).then((group)=>{
                group.setUsers(user);
            });
        })
        .catch((error)=>{
            Logger.error(error);
        });
    });
}