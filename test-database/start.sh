#!/bin/sh

# Run the MySQL container, with a database named 'employee' and credentials
# for a users-service user which can access it.
# echo "Starting DB..."
# docker run --name db -d \
#   -e MYSQL_ROOT_PASSWORD='developer' \
#   -e MYSQL_DATABASE=employee -e MYSQL_USER=users_service -e MYSQL_PASSWORD='developer' \
#   -p 3306:3306 \
#   mysql:latest

# Wait for the database service to start up.
echo "Waiting for DB to start up..."
docker exec db mysqladmin --silent --wait=30 -uusers_service -pdeveloper ping || exit 1
